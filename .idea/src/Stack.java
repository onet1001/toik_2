import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Stack implements StackOperations {

    private List<String> stackOfStrings = new ArrayList<>();


    @Override
    public List<String> get() {
        return stackOfStrings;
    }

    @Override
    public Optional<String> pop() {

        if(this.stackOfStrings.size() == 0){
            return Optional.empty();
        }else{

            String element = stackOfStrings.get(0);

            stackOfStrings.remove(0);

            return Optional.of(element);
        }
    }

    @Override
    public void push(String item) {
        this.stackOfStrings.add(item);
    }
}
