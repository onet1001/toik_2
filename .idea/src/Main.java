public class Main {

    public static void main(String[] args){
        Stack stack = new Stack();
        System.out.println(stack.get());
        stack.push("a");
        stack.push("b");
        stack.push("c");
        System.out.println(stack.get());
        stack.pop();
        System.out.println(stack.get());
    }

}
